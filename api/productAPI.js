import express from "express";
import { get,create,update,destroy,find } from "../controller/productController.js";

const router = express.Router()

router.get('/list', get)
router.post('/create', create)
router.put('/update', update)
router.delete('/delete', destroy)
router.get('/find', find)

export default router