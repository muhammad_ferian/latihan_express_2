import express from "express";
import { create,list,update,find,destroy } from "../controller/orderConreoller.js";
import { setId } from "../middleware/updateMiddleware.js";

const router = express.Router();

router.get('/list', list);
router.post('/create', create);
router.get('/find/:id', find);
router.put('/update/:id', setId, update);
router.delete('/delete/:id', destroy)

export default router;