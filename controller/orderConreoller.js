import Order from "../resources/order.js";

export const create = (req, res) => {
    const order = Order.create(req.body)

    return res.status(200).json(order)
}

export const list = (req, res) => {
    const orders = Order.list()

    return res.status(200).json(orders)
}

export const find = (req, res) => {
    const order = Order.find(req.params.id)

    return res.status(200).json(order)
}

export const update = (req, res) => {
    const order = Order.update(req.body)

    return res.status(200).json(order)
}

export const destroy = (req, res) => {
    const order = Order.delete(req.params.id)

    return res.status(200).json(order)
}