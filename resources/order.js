const orders = 
[
    // {
    //     "id": 1,
    //     "name": "ferian",
    //     "address": "blitar",
    //     "order": "keyboard",
    //     "price": 500000
    // },
    // {
    //     "id": 2,
    //     "name": "akbar",
    //     "address": "malang",
    //     "order": "monitor",
    //     "price": 1500000
    // },
    // {
    //     "id": 3,
    //     "name": "irfan",
    //     "address": "surabaya",
    //     "order": "mouse",
    //     "price": 300000
    // }
];

export default {
    create(params) {
        const order = {
            id : params.id,
            name : params.name,
            address : params.address,
            order : params.order,
            price : params.price
        }

        orders.push(order);

        return order;
    },

    list() {
        return orders;
    },

    find(id) {
        const order = orders.find((i) => i,id === Number(id));
        if(!order) {
            return null;
        } else {
            return order;
        }
    },

    delete(id) {
        const order = orders.find((i) => i,id === Number(id));
        if(!order) {
            return null;
        } else {
            orders.splice(id - 1, 1);
            return `Data Deleted`;
        }
    },

    update(params) {
        const order = orders.findIndex((i) => i.id === this.id);

        params.id && (this.id = params.id);
        params.name && (this.ordnameer = params.name);
        params.address && (this.address = params.address);
        params.order && (this.order = params.order);
        params.price && (this.price = params.price);

        orders[order] = this;

        return this;
    }
}