import Order from "../resources/order.js";

function setId(req, res, next) {
    const order = Order.find(req.params.id)
    if(!order) {
        return null;
    } else {
        req.order = order;
        next();
    }
}

export { setId }