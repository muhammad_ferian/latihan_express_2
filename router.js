import express from "express";
import { checkHealth, showBio } from './controller/basicController.js';
import { authMiddleware } from './middleware/basicMiddleware.js'
// import { get as getProduct, create as createProduct, update as updateProduct, 
//     destroy as destroyProduct, find as findProduct } from "./controller/productController.js";
import productAPI from './api/productAPI.js';
import { viewRender } from './controller/viewsController.js';
import orderAPI from './api/orderAPI.js'

const router = express.Router()

router.get('/check-health', checkHealth)
router.get('/biodata', authMiddleware, showBio)
router.use('/product', productAPI)
router.use('/order', orderAPI)
// views
router.get('/views', viewRender)


// router.get('/product/list', getProduct)
// router.post('/product/create', createProduct)
// router.put('/product/update', updateProduct)
// router.delete('/product/delete', destroyProduct)
// router.get('/product/find', findProduct)

export default router